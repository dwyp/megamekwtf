package megamek.discord;

import discord4j.common.util.Snowflake;
import discord4j.core.DiscordClientBuilder;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.object.entity.channel.MessageChannel;
import megamek.common.GameTurn;
import megamek.common.IGame;
import megamek.common.IPlayer;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DiscordBot {
    private static final Long CHANNEL = 747570667800494081L;
    private static final String API_KEY = "NzYwNDc3NzUwODc1NjUyMTQ2.X3MoGA.9vGRNPoLjT3_2LD5PfYm9_HNOVA";

    private final GatewayDiscordClient client = getClient();
    private final Map<String, String> discordUserIdMap = new HashMap<String, String>() {
        {
            put("Dan", "<@175820001658404865>");
            put("Scott", "<@326929172448411649>");
            put("Tony", "<@131939323376435200>");
            put("Anil", "<@133746860375408640>");
            put("Jason", "<@401562899471925249>");
            put("Steve", "<@410118795907170315>");
        }
    };

    private IGame.Phase lastPhase = IGame.Phase.PHASE_LOUNGE;
    private String lastPlayer = "";

    public void sendMentions(IGame game, IPlayer player) {
        if (game.getPhase() != lastPhase) {
            sendSeparator(getChannel());
        }

        if (game.isPhaseSimultaneous()) {
            if (!lastPhase.equals(game.getPhase())) {
                for (String playerName: getPlayersFromTurns(game)) {
                    sendMention(playerName, game.getPhase(), getChannel());
                }
            }
        } else if (!isRepeatMessage(player.getName(), game.getPhase())) {
            sendMention(player.getName(), game.getPhase(), getChannel());
        }

        lastPlayer = player.getName();
        lastPhase = game.getPhase();
    }

    private boolean isRepeatMessage(String playerName, IGame.Phase currentPhase) {
        return (lastPlayer.equals(playerName) && lastPhase.equals((currentPhase)));
    }

    private List<String> getPlayersFromTurns(IGame game) {
        List<String> playerList = new ArrayList<>();
        for (GameTurn gameTurn : game.getTurnVector()) {
            String playerName = game.getPlayer(gameTurn.getPlayerNum()).getName();
            if (!playerList.contains(playerName)) {
                playerList.add(playerName);
            }
        }
        return playerList;
    }

    private GatewayDiscordClient getClient() {
        return DiscordClientBuilder.create(API_KEY)
                .build()
                .login()
                .block();
    }

    private Mono<MessageChannel> getChannel() {
        return client.getChannelById(Snowflake.of(CHANNEL)).cast(MessageChannel.class);
    }

    private void sendMention(String name, IGame.Phase phase, Mono<MessageChannel> channel) {
        if (discordUserIdMap.containsKey(name)) {
            channel.flatMap(ch -> ch.createMessage(discordUserIdMap.get(name) + " It is " + name + "'s turn: "
                    + IGame.Phase.getDisplayableName(phase) + " Phase")).subscribe();
        }
    }

    private void sendSeparator(Mono<MessageChannel> channel) {
        channel.flatMap(ch -> ch.createMessage("---------------------")).subscribe();
    }
}
