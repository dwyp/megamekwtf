package megamek.server.reports.stats;

import megamek.common.Entity;
import megamek.common.IGame;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class InitialBVStore {
    private final IGame game;
    private final Map<Integer, Integer> bvMap = new HashMap<>();

    public InitialBVStore(IGame game) {
        this.game = game;
        init();
    }

    private void init() {
        Iterator<Entity> iterator = game.getEntities();
        while (iterator.hasNext()) {
            Entity entity = iterator.next();
            bvMap.put(entity.getId(), entity.getInitialBV());
        }
    }

    public int getBV(String unit) {
        int unitId = getId(unit);
        if (bvMap.get(unitId) != null) {
            return bvMap.get(unitId);
        }

        Iterator<Entity> iterator = game.getEntities();
        while (iterator.hasNext()) {
            Entity entity = iterator.next();
            if (unitId ==  entity.getId()) {
                bvMap.put(unitId, entity.getInitialBV());
                return entity.getInitialBV();
            }
        }

        return 0;
    }

    private int getId(String unit) {
        if (!unit.contains("#entity:")) { return -1; }
        return Integer.parseInt(unit.substring(unit.indexOf(":") + 1, unit.indexOf("\">")));
    }
}
