package megamek.server.reports.stats;

import java.util.*;

public class UnitStats {
    private final String playerName;
    private final String unit;
    private final int bv;

    // attack stats
    private int wHits;
    private int wMisses;
    private int pHits;
    private int pMisses;
    private int weaponDamageDone;
    private int physicalDamageDone;
    private final Map<String, Integer> weaponTypeAttackCount = new HashMap<>();
    private final Map<String, Integer> weaponTypeHitCount = new HashMap<>();
    private final Map<String, Integer> weaponTypeDamage = new HashMap<>();
    private final Map<String, Integer> hitLocationCount = new HashMap<>();
    private final Map<String, Integer> hitLocationDamage = new HashMap<>();
    private final List<Integer> toHitTN = new ArrayList<>();
    private final List<Integer> toHitRolls = new ArrayList<>();

    // target stats
    private final Map<String, Integer> targetAttackCount = new HashMap<>();
    private final Map<String, Integer> targetDamageDone = new HashMap<>();

    // defense stats
    private int wDodges;
    private int wHitsTaken;
    private int pDodges;
    private int pHitsTaken;
    private int weaponDamageReceived;
    private int physicalDamageReceived;

    // attacker stats
    private final Map<String, Integer> attackerAttackCount = new HashMap<>();
    private final Map<String, Integer> attackerDamageDone = new HashMap<>();

    public UnitStats(String playerName, String unit, int bv) {
        this.playerName = playerName;
        this.unit = unit;
        this.bv = bv;
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getUnit() {
        return unit;
    }

    public int getBV() { return bv; }

    public int getWHits() {
        return wHits;
    }

    public void addToWHits(int wHits) {
        this.wHits += wHits;
    }

    public int getWMisses() {
        return wMisses;
    }

    public void addToWMisses(int wMisses) {
        this.wMisses += wMisses;
    }

    public int getPHits() { return pHits; }

    public void addToPHits(int pHits) { this.pHits += pHits; }

    public int getPMisses() { return pMisses; }

    public void addToPMisses(int pMisses) { this.pMisses += pMisses; }

    public int getWeaponDamageDone() {
        return weaponDamageDone;
    }

    public void addToWeaponDamageDone(int weaponDamageDone) {
        this.weaponDamageDone += weaponDamageDone;
    }

    public int getPhysicalDamageDone() {
        return physicalDamageDone;
    }

    public void addToPhysicalDamageDone(int physicalDamageDone) {
        this.physicalDamageDone += physicalDamageDone;
    }

    public int getTotalDamageDone() { return physicalDamageDone + weaponDamageDone; }

    public Map<String, Integer> getWeaponTypeAttackCount() {
        return weaponTypeAttackCount;
    }

    public void addToWeaponTypeAttackCount(String weaponType, Integer attacksToAdd) {
        Integer currentCount = weaponTypeAttackCount.computeIfAbsent(weaponType, k -> 0);
        weaponTypeAttackCount.put(weaponType, currentCount + attacksToAdd);
    }

    public Map<String, Integer> getWeaponTypeHitCount() {
        return weaponTypeHitCount;
    }

    public void addToWeaponTypeHitCount(String weaponType, Integer hitsToAdd) {
        Integer currentCount = weaponTypeAttackCount.computeIfAbsent(weaponType, k -> 0);
        weaponTypeHitCount.put(weaponType, currentCount + hitsToAdd);
    }

    public Map<String, Integer> getWeaponTypeDamage() {
        return weaponTypeDamage;
    }

    public void addToWeaponTypeDamage(String weaponType, Integer damageToAdd) {
        Integer currentCount = weaponTypeDamage.computeIfAbsent(weaponType, k -> 0);
        weaponTypeDamage.put(weaponType, currentCount + damageToAdd);
    }

    public Map<String, Integer> getHitLocationCount() {
        return hitLocationCount;
    }

    public void addToHitLocationCount(String hitLocation, Integer hitsToAdd) {
        Integer currentCount = hitLocationCount.computeIfAbsent(hitLocation, k -> 0);
        hitLocationCount.put(hitLocation, currentCount + hitsToAdd);
    }

    public Map<String, Integer> getHitLocationDamage() {
        return hitLocationDamage;
    }

    public void addToHitLocationDamage(String hitLocation, Integer damageToAdd) {
        Integer currentCount = hitLocationDamage.computeIfAbsent(hitLocation, k -> 0);
        hitLocationDamage.put(hitLocation, currentCount + damageToAdd);
    }

    public List<Integer> getToHitTN() {
        return toHitTN;
    }

    public void addToHitTN(Integer toHitTN) {
        this.toHitTN.add(toHitTN);
    }

    public List<Integer> getToHitRolls() {
        return toHitRolls;
    }

    public void addToHitRoll(Integer toHitRoll) {
        toHitRolls.add(toHitRoll);
    }

    // Getters that return the average
    public OptionalDouble getAvgTN() {
        return toHitTN.stream().mapToInt(Integer::intValue).average();
    }

    public OptionalDouble getAvgToHitRoll() {
        return toHitRolls.stream().mapToInt(Integer::intValue).average();
    }

    public double getAvgWeaponDmg() {
        if (pHits + wHits > 0) {
            return (double)(weaponDamageDone + physicalDamageDone) / (pHits + wHits);
        } else return 0;
    }


    // Target stats
    public Map<String, Integer> getTargetAttackCount() {
        return targetAttackCount;
    }

    public void addToTargetAttackCount(String target, Integer attacksToAdd) {
        Integer currentCount = targetAttackCount.computeIfAbsent(target, k -> 0);
        targetAttackCount.put(target, currentCount + attacksToAdd);
    }

    public Map<String, Integer> getTargetDamageDone() {
        return targetDamageDone;
    }

    public void addToTargetDamageDone(String target, Integer damageToAdd) {
        Integer currentCount = targetDamageDone.computeIfAbsent(target, k -> 0);
        targetDamageDone.put(target, currentCount + damageToAdd);
    }

    // Defense stats
    public int getWDodges() {
        return wDodges;
    }

    public void addToWDodges(int wDodges) {
        this.wDodges += wDodges;
    }

    public int getWHitsTaken() { return wHitsTaken;
    }

    public void addToWHitsTaken(int wHitsTaken) {
        this.wHitsTaken += wHitsTaken;
    }

    public int getPDodges() { return pDodges; }

    public void addToPDodges(int pDodges) { this.pDodges += pDodges; }

    public int getPHitsTaken() { return pHitsTaken; }

    public void addToPHitsTaken(int pHitsTaken) { this.pHitsTaken += pHitsTaken; }

    public int getWeaponDamageReceived() {
        return weaponDamageReceived;
    }

    public void addToWeaponDamageReceived(int weaponDamageReceived) {
        this.weaponDamageReceived += weaponDamageReceived;
    }

    public int getPhysicalDamageReceived() {
        return physicalDamageReceived;
    }

    public void addToPhysicalDamageReceived(int physicalDamageReceived) {
        this.physicalDamageReceived += physicalDamageReceived;
    }

    public int getTotalDamageReceived() { return physicalDamageReceived + weaponDamageReceived; }

    // Attacked by stats
    public Map<String, Integer> getAttackerAttackCount() {
        return attackerAttackCount;
    }

    public void addToAttackerAttackCount(String attacker, Integer attacksToAdd) {
        Integer currentCount = attackerAttackCount.computeIfAbsent(attacker, k -> 0);
        attackerAttackCount.put(attacker, currentCount + attacksToAdd);
    }

    public Map<String, Integer> getAttackerDamageDone() {
        return attackerDamageDone;
    }

    public void addToAttackerDamageDone(String attacker, Integer damageToAdd) {
        Integer currentCount = attackerDamageDone.computeIfAbsent(attacker, k -> 0);
        attackerDamageDone.put(attacker, currentCount + damageToAdd);
    }
}
