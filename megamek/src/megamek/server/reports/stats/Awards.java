package megamek.server.reports.stats;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Awards {
    public String getAwards(List<UnitStats> stats) {
        Optional<UnitStats> unitStats;
        UnitStats emptyUnit = new UnitStats("", "", 0);
        StringBuilder sb = new StringBuilder();

        stats = stats.stream().filter(k -> !k.getUnit().contains("Pilot") && !k.getUnit().contains("Hex: ")).collect(Collectors.toList());

        //        Most Damage		    Devastator
        unitStats = stats.stream().max(Comparator.comparingInt(unit -> unit.getPhysicalDamageDone() + unit.getWeaponDamageDone()));
        sb.append(unitStats.orElse(emptyUnit).getPlayerName()).append(" ").append(unitStats.orElse(emptyUnit).getUnit());
        sb.append(" is a <B>DEVASTATOR</B>!!! (Most Damage)\n");

        //        Least Damage		    Water Pistol
        unitStats = stats.stream().min(Comparator.comparingInt(unit -> unit.getPhysicalDamageDone() + unit.getWeaponDamageDone()));
        sb.append(unitStats.orElse(emptyUnit).getPlayerName()).append(" ").append(unitStats.orElse(emptyUnit).getUnit());
        sb.append(" is a <B>Water Pistol</B>. (Least Damage)\n");

        //        Highest Dodge Ratio	Dancer
        unitStats =  stats.stream().max(Comparator.comparingInt(unit ->
                getMaxPercentage(unit.getPDodges()+unit.getWDodges(), unit.getPHitsTaken()+unit.getWHitsTaken())));
        sb.append(unitStats.orElse(emptyUnit).getPlayerName()).append(" ").append(unitStats.orElse(emptyUnit).getUnit());
        sb.append(" is a <B>DANCER</B>!!! (Best Dodge Ratio)\n");

        //        Lowest Dodge Ratio    China Shop Bull
        unitStats = stats.stream().min(Comparator.comparingInt(unit ->
                getMinPercentage(unit.getPDodges()+unit.getWDodges(), unit.getPHitsTaken()+unit.getWHitsTaken())));
        sb.append(unitStats.orElse(emptyUnit).getPlayerName()).append(" ").append(unitStats.orElse(emptyUnit).getUnit());
        sb.append(" is a <B>China Shop Bull</B>. (Worst Dodge Ratio)\n");

        //        Most Damage Taken		Panzer
        unitStats = stats.stream().max(Comparator.comparingInt(unit -> unit.getPhysicalDamageReceived() + unit.getWeaponDamageReceived()));
        sb.append(unitStats.orElse(emptyUnit).getPlayerName()).append(" ").append(unitStats.orElse(emptyUnit).getUnit());
        sb.append(" is a <B>PANZER</B>!!! (Most Damage Taken)\n");

        //        Least Damage Taken	Skulker
        unitStats = stats.stream().min(Comparator.comparingInt(unit -> unit.getPhysicalDamageReceived() + unit.getWeaponDamageReceived()));
        sb.append(unitStats.orElse(emptyUnit).getPlayerName()).append(" ").append(unitStats.orElse(emptyUnit).getUnit());
        sb.append(" is a <B>Skulker</B>. (Least Damage Taken)\n");

        //        Most Weapon Attacks	Gatling Gun
        unitStats = stats.stream().max(Comparator.comparingInt(unit -> unit.getWHits() + unit.getWMisses() + unit.getPHits() + unit.getPMisses()));
        sb.append(unitStats.orElse(emptyUnit).getPlayerName()).append(" ").append(unitStats.orElse(emptyUnit).getUnit());
        sb.append(" is a <B>GATLING GUN</B>!!! (Most Attacks)\n");

        //        Least Weapon Attacks  Leisurely
        unitStats = stats.stream().min(Comparator.comparingInt(unit -> unit.getWHits() + unit.getWMisses() + unit.getPHits() + unit.getPMisses()));
        sb.append(unitStats.orElse(emptyUnit).getPlayerName()).append(" ").append(unitStats.orElse(emptyUnit).getUnit());
        sb.append(" is <B>Leisurely</B>. (Least Attacks)\n");

        //        Most Physical Damage	Gladiator
        unitStats = stats.stream().max(Comparator.comparingInt(UnitStats::getPhysicalDamageDone));
        sb.append(unitStats.orElse(emptyUnit).getPlayerName()).append(" ").append(unitStats.orElse(emptyUnit).getUnit());
        sb.append(" is a <B>GLADIATOR</B>!!! (Most Physical Damage)\n");

        //        Best To Hit Rolls		Favored
        unitStats = stats.stream().max(Comparator.comparingInt(unit -> (int) unit.getAvgToHitRoll().orElse(0)*100));
        sb.append(unitStats.orElse(emptyUnit).getPlayerName()).append(" ").append(unitStats.orElse(emptyUnit).getUnit());
        sb.append(" is <B>FAVORED</B>!!! (Best To Hit Rolls)\n");

        //        Worst To Hit Rolls	Jinxed
        unitStats = stats.stream().min(Comparator.comparingInt(unit -> (int) unit.getAvgToHitRoll().orElse(100)*100));
        sb.append(unitStats.orElse(emptyUnit).getPlayerName()).append(" ").append(unitStats.orElse(emptyUnit).getUnit());
        sb.append(" is <B>Jinxed</B>. (Worst To Hit Rolls)\n");

        //        Highest TN		    Daring
        unitStats = stats.stream().max(Comparator.comparingInt(unit -> (int) unit.getAvgTN().orElse(0)*100));
        sb.append(unitStats.orElse(emptyUnit).getPlayerName()).append(" ").append(unitStats.orElse(emptyUnit).getUnit());
        sb.append(" is <B>DARING</B>!!! (Highest TNs)\n");

        //        Lowest TN		        Bet Hedger
        unitStats = stats.stream().min(Comparator.comparingInt(unit -> (int) unit.getAvgTN().orElse(100)*100));
        sb.append(unitStats.orElse(emptyUnit).getPlayerName()).append(" ").append(unitStats.orElse(emptyUnit).getUnit());
        sb.append(" is a <B>Bet Hedger</B>. (Lowest TNs)");

        return sb.toString();
    }

    private int getMaxPercentage(int x, int y) {
        if (y + x == 0 ) return -1;
        return (x * 100 / (x + y));
    }

    private int getMinPercentage(int x, int y) {
        if (y + x == 0 ) return 101;
        return (x * 100 / (x + y));
    }
}
