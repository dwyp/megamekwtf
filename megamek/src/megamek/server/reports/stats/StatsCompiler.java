package megamek.server.reports.stats;

import megamek.common.IGame;
import megamek.common.Report;
import megamek.server.reports.ParsedReports;
import megamek.server.reports.WeaponReport;
import megamek.server.reports.physicalreport.ParsedPhysicalReport;

import java.util.*;

public class StatsCompiler {
    private final InitialBVStore initialBvStore;
    private final StatsTables statsTables;
    private final Awards awards;

    public StatsCompiler(IGame game) {
        this.initialBvStore = new InitialBVStore(game);
        this.statsTables = new StatsTables();
        this.awards = new Awards();
    }

    public Map<String, UnitStats> getUnitStats(ParsedReports parsedFiringPhaseReports,
                                               ParsedPhysicalReport parsedPhysicalPhaseReports) {
        Map<Integer, ParsedReports> parsedFiringPhaseReportsMap = new HashMap<>();
        parsedFiringPhaseReportsMap.put(1, parsedFiringPhaseReports);
        Map<Integer, ParsedPhysicalReport> parsedPhysicalPhaseReportsMap = new HashMap<>();
        parsedPhysicalPhaseReportsMap.put(1, parsedPhysicalPhaseReports);

        return getUnitStats(parsedFiringPhaseReportsMap, parsedPhysicalPhaseReportsMap);
    }

    public Map<String, UnitStats> getUnitStats(Map<Integer, ParsedReports> parsedFiringPhaseReportsMap,
                                               Map<Integer, ParsedPhysicalReport> parsedPhysicalPhaseReportsMap) {
        Map<String, UnitStats> stats = new HashMap<>();

        collectFiringPhaseStats(stats, parsedFiringPhaseReportsMap);
        collectPhysicalPhaseStats(stats, parsedPhysicalPhaseReportsMap);

        return stats;
    }

    private void collectFiringPhaseStats(Map<String, UnitStats> stats,
                                         Map<Integer, ParsedReports> parsedFiringPhaseReportsMap) {
        for (ParsedReports parsedReport : parsedFiringPhaseReportsMap.values()) {
            for (WeaponReport wr : parsedReport.getWeaponReports()) {
                // attacker stats
                UnitStats unitStats = stats.computeIfAbsent(wr.getAttackerUnit(),
                        k -> new UnitStats(wr.getAttackerPlayerName(), wr.getAttackerUnit(),
                                initialBvStore.getBV(wr.getAttackerUnit())));

                if (wr.isHit()) {
                    unitStats.addToWHits(1);
                } else {
                    unitStats.addToWMisses(1);
                }

                unitStats.addToWeaponDamageDone(wr.getDamages().stream().reduce(0, java.lang.Integer::sum));

                for (int index = 0; index < wr.getHitLocations().size(); index++) {
                    String hitLocation = wr.getHitLocations().get(index);
                    unitStats.addToHitLocationCount(hitLocation, 1);
                    unitStats.addToHitLocationDamage(hitLocation, wr.getDamages().get(index));
                }

                unitStats.addToHitRoll(wr.getHitRoll());
                unitStats.addToHitTN(wr.getNeededToHit());
                unitStats.addToTargetAttackCount(wr.getPlayerAttacked() + " " + wr.getUnitAttacked(), 1);

                // defender stats
                unitStats = stats.computeIfAbsent(wr.getUnitAttacked(),
                        k -> new UnitStats(wr.getPlayerAttacked(), wr.getUnitAttacked(),
                                initialBvStore.getBV(wr.getUnitAttacked())));

                if (wr.isHit()) {
                    unitStats.addToWHitsTaken(1);
                } else unitStats.addToWDodges(1);

                unitStats.addToWeaponDamageReceived(wr.getDamages().stream().reduce(0, java.lang.Integer::sum));
            }
        }
    }


    private void collectPhysicalPhaseStats(Map<String, UnitStats> stats,
                               Map<Integer, ParsedPhysicalReport> parsedPhysicalPhaseReportsMap) {

        for (ParsedPhysicalReport parsedReport : parsedPhysicalPhaseReportsMap.values()) {
            for (WeaponReport wr : parsedReport.getWeaponReports()) {
                // attacker stats
                UnitStats unitStats = stats.computeIfAbsent(wr.getAttackerUnit(),
                        k -> new UnitStats(wr.getAttackerPlayerName(), wr.getAttackerUnit(),
                                initialBvStore.getBV(wr.getAttackerUnit())));

                if (wr.isHit()) {
                    unitStats.addToPHits(1);
                } else {
                    unitStats.addToPMisses(1);
                }

                unitStats.addToPhysicalDamageDone(wr.getDamages().stream().reduce(0, java.lang.Integer::sum));

                for (int index = 0; index < wr.getHitLocations().size(); index++) {
                    String hitLocation = wr.getHitLocations().get(index);
                    unitStats.addToHitLocationCount(hitLocation, 1);
                    unitStats.addToHitLocationDamage(hitLocation, wr.getDamages().get(index));
                }

                unitStats.addToHitRoll(wr.getHitRoll());
                unitStats.addToHitTN(wr.getNeededToHit());
                unitStats.addToTargetAttackCount(wr.getPlayerAttacked() + " " + wr.getUnitAttacked(), 1);

                // defender stats
                unitStats = stats.computeIfAbsent(wr.getUnitAttacked(),
                        k -> new UnitStats(wr.getPlayerAttacked(), wr.getUnitAttacked(),
                                initialBvStore.getBV(wr.getUnitAttacked())));

                if (wr.isHit()) {
                    unitStats.addToPHitsTaken(1);
                } else unitStats.addToPDodges(1);

                unitStats.addToPhysicalDamageReceived(wr.getDamages().stream().reduce(0, java.lang.Integer::sum));
            }
        }
    }

    public void addRoundStats(ParsedReports parsedFiringPhaseReports,
                              ParsedPhysicalReport parsedPhysicalPhaseReports,
                              Vector<Report> vPhaseReport) {

        Map<String, UnitStats> stats = getUnitStats(parsedFiringPhaseReports, parsedPhysicalPhaseReports);

        String[][] statsArray = statsTables.createSecondArrayOfStats(statsTables.sortStats(StatsTables.W_DAMAGE, stats));
        Report r = new Report(1230, Report.PUBLIC);
        r.add(statsTables.arrayToHTMLTable(statsArray));
        vPhaseReport.add(r);
    }

    public void addStats(Map<Integer, ParsedReports> parsedFiringPhaseReportsMap,
                         Map<Integer, ParsedPhysicalReport> parsedPhysicalPhaseReportsMap,
                         Vector<Report> vPhaseReport) {
        Map<String, UnitStats> stats = getUnitStats(parsedFiringPhaseReportsMap, parsedPhysicalPhaseReportsMap);

        String[][] statsArray = statsTables.createFirstArrayOfStats(statsTables.sortStats(StatsTables.T_DAMAGE, stats));
        Report r = new Report(1230, Report.PUBLIC);
        r.add(statsTables.arrayToHTMLTable(statsArray));
        vPhaseReport.add(r);

        statsArray = statsTables.createSecondArrayOfStats(statsTables.sortStats(StatsTables.W_DAMAGE, stats));
        r = new Report(1230, Report.PUBLIC);
        r.add(statsTables.arrayToHTMLTable(statsArray));
        vPhaseReport.add(r);


        statsArray = statsTables.createThirdArrayOfStats(statsTables.sortStats(StatsTables.P_DAMAGE, stats));
        r = new Report(1230, Report.PUBLIC);
        r.add(statsTables.arrayToHTMLTable(statsArray));
        vPhaseReport.add(r);
    }

    public void addAwards(Map<Integer, ParsedReports> parsedFiringPhaseReportsMap,
                          Map<Integer, ParsedPhysicalReport> parsedPhysicalPhaseReportsMap,
                          Vector<Report> vPhaseReport) {
        List<UnitStats> stats = new ArrayList<>(getUnitStats(parsedFiringPhaseReportsMap, parsedPhysicalPhaseReportsMap).values());
        Report r = new Report(1230, Report.PUBLIC);
        r.add(awards.getAwards(stats));
        vPhaseReport.add(r);
    }
}

