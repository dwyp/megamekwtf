package megamek.server.reports.stats;

import megamek.common.Report;
import megamek.server.reports.ParsedReports;
import megamek.server.reports.physicalreport.ParsedPhysicalReport;

import java.util.*;

public class StatsTables {
    public static final int T_DAMAGE = 1;
    public static final int W_DAMAGE = 2;
    public static final int P_DAMAGE = 3;

    public List<UnitStats> sortStats(int sortBy, Map<String, UnitStats> stats) {
        List<UnitStats> unitStatsList = new ArrayList<>(stats.values());
        switch(sortBy) {
            case T_DAMAGE : unitStatsList.sort(Comparator.comparingInt(UnitStats::getTotalDamageDone).reversed());
                break;
            case W_DAMAGE : unitStatsList.sort(Comparator.comparingInt(UnitStats::getWeaponDamageDone).reversed());
                break;
            case P_DAMAGE : unitStatsList.sort(Comparator.comparingInt(UnitStats::getPhysicalDamageDone).reversed());
                break;
        }

        return unitStatsList;
    }

    public String[][] createFirstArrayOfStats(List<UnitStats> stats) {
        String[] header = {"Player","Unit","T.Dmg","T.Dmg Taken","T.Dmg/BV","T.Dmg Taken/BV","Avg Roll","Avg TN","Avg Dmg", "# Attacks", "Favorite Target"};
        String[][] array = new String[stats.size() + 1][header.length];
        System.arraycopy(header, 0, array[0], 0, header.length);
        int index = 1;
        for(UnitStats stat : stats) {
            if (stat.getPhysicalDamageDone()+stat.getWeaponDamageDone() > 0
                    || stat.getPhysicalDamageReceived()+stat.getWeaponDamageReceived() > 0
                    || stat.getWMisses()+stat.getPMisses() > 0) {
                array[index][0] = stat.getPlayerName();
                array[index][1] = stat.getUnit();
                array[index][2] = java.lang.String.valueOf(stat.getTotalDamageDone());
                array[index][3] = java.lang.String.valueOf(stat.getTotalDamageReceived());
                array[index][4] = getDivisionAsString(stat.getTotalDamageDone(), ((stat.getBV())));
                array[index][5] = getDivisionAsString(stat.getTotalDamageReceived(), ((stat.getBV())));
                array[index][6] = getAverageAsString(stat.getAvgToHitRoll());
                array[index][7] = getAverageAsString(stat.getAvgTN());
                array[index][8] = java.lang.String.format("%.2f", stat.getAvgWeaponDmg());
                array[index][9] = java.lang.String.valueOf(stat.getPHits() + stat.getPMisses() + stat.getWHits() + stat.getWMisses());
                array[index][10] = java.lang.String.valueOf(keyForMaxValue(stat.getTargetAttackCount()));
                index++;
            }
        }
        return array;
    }

    public String[][] createSecondArrayOfStats(List<UnitStats> stats) {
        String[] header = {"Player","Unit","W.Dmg","W.Dmg Taken","W.Hits","W.Misses","Hit %","W.Dodges","W.Hits Taken","W.Dodge %"};
        String[][] array = new String[stats.size() + 1][header.length];
        System.arraycopy(header, 0, array[0], 0, header.length);
        int index = 1;
        for(UnitStats stat : stats) {
            if (stat.getWeaponDamageDone() > 0
                    || stat.getWeaponDamageReceived() > 0
                    || stat.getWMisses() > 0
                    || stat.getWDodges() > 0) {
                array[index][0] = stat.getPlayerName();
                array[index][1] = stat.getUnit();
                array[index][2] = java.lang.String.valueOf(stat.getWeaponDamageDone());
                array[index][3] = java.lang.String.valueOf(stat.getWeaponDamageReceived());
                array[index][4] = java.lang.String.valueOf(stat.getWHits());
                array[index][5] = java.lang.String.valueOf(stat.getWMisses());
                array[index][6] = getPercentageAsString(stat.getWHits(), stat.getWMisses());
                array[index][7] = java.lang.String.valueOf(stat.getWDodges());
                array[index][8] = java.lang.String.valueOf(stat.getWHitsTaken());
                array[index][9] = getPercentageAsString(stat.getWDodges(), stat.getWHitsTaken());
                index++;
            }
        }
        return array;
    }

    public String[][] createThirdArrayOfStats(List<UnitStats> stats) {
        String[] header = {"Player","Unit","P.Dmg","P.Dmg Taken","P.Hits","P.Misses","Hit %","P.Dodges","P.Hits Taken","P.Dodge %"};
        String[][] array = new String[stats.size() + 1][header.length];
        System.arraycopy(header, 0, array[0], 0, header.length);
        int index = 1;
        for(UnitStats stat : stats) {
            if (stat.getPhysicalDamageDone() > 0
                    || stat.getPhysicalDamageReceived() > 0
                    || stat.getPMisses() > 0
                    || stat.getPDodges() > 0) {
                array[index][0] = stat.getPlayerName();
                array[index][1] = stat.getUnit();
                array[index][2] = java.lang.String.valueOf(stat.getPhysicalDamageDone());
                array[index][3] = java.lang.String.valueOf(stat.getPhysicalDamageReceived());
                array[index][4] = java.lang.String.valueOf(stat.getPHits());
                array[index][5] = java.lang.String.valueOf(stat.getPMisses());
                array[index][6] = getPercentageAsString(stat.getPHits(), stat.getPMisses());
                array[index][7] = java.lang.String.valueOf(stat.getPDodges());
                array[index][8] = java.lang.String.valueOf(stat.getPHitsTaken());
                array[index][9] = getPercentageAsString(stat.getPDodges(), stat.getPHitsTaken());
                index++;
            }
        }
        return array;
    }

    public String arrayToHTMLTable(Object[][] array) {
        StringBuilder sb = new StringBuilder("\n<table border=\"1\">");
        for (Object elem : array[0]) {
            if (elem != null) {
                sb.append("<th>").append(elem.toString()).append("</th>");
            }
        }
        for (int i = 1; i < array.length; i++) {
            if (array[i][0] != null) {
                Object[] row = array[i];
                sb.append("<tr>");
                for (Object elem : row) {
                    if (elem != null) {
                        sb.append("<td>").append(elem.toString()).append("</td>");
                    }
                }
                sb.append("</tr>");
            }
        }
        sb.append("</table>\n");
        return sb.toString();
    }

    private String keyForMaxValue(Map<String, Integer> map) {
        Optional<Map.Entry<String, Integer>> maxEntry = map.entrySet()
                .stream()
                .max(Map.Entry.comparingByValue());
        return maxEntry.orElse(new AbstractMap.SimpleEntry<>("", 0)).getKey();
    }

    private String getAverageAsString(OptionalDouble optionalDouble) {
        if (optionalDouble.isPresent()) {
            return java.lang.String.format("%.2f", optionalDouble.getAsDouble());
        } else return "";
    }

    private String getDivisionAsString(int x, int y) {
        if (y == 0) return "";
        return java.lang.String.format("%.2f", ((double)x / y) * 1000);

    }

    private String getPercentageAsString(int x, int y) {
        if (y + x == 0 ) return "";
        return (x * 100 / (x + y)) + "%";
    }
}

