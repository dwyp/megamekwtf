package megamek.server.reports;

import java.util.ArrayList;
import java.util.List;

public class WeaponsFireReport {
    private int weaponsFireReportKey;
    private int start;
    private int end;
    private final String playerFiring;
    private final String unitFiring;
    private int damage;
    private int hits;
    private int misses;
    private String hitMissRatio;
    // TODO: use this and get rid of weaponsFireReportKey
    private final List<WeaponReport> weaponReportsForThisWeaponsFireReport = new ArrayList<>();

    public WeaponsFireReport(int weaponsFireReportKey, int start, int end, String playerFiring, String unitFiring) {
        this.weaponsFireReportKey = weaponsFireReportKey;
        this.start = start;
        this.end = end;
        this.playerFiring = playerFiring;
        this.unitFiring = unitFiring;
    }

    public int getWeaponsFireReportKey() {
        return weaponsFireReportKey;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public String getPlayerFiring() {
        return playerFiring;
    }

    public String getUnitFiring() {
        return unitFiring;
    }

    public Integer getTotalDamage() {
        return damage;
    }

    public void addDamage(Integer damage) {
        this.damage += damage;
    }

    public int getHits() {
        return hits;
    }

    public void addHits(int hits) {
        this.hits += hits;
    }

    public int getMisses() {
        return misses;
    }

    public void addMisses(int misses) {
        this.misses += misses;
    }

    public String getHitMissRatio() {
        return hitMissRatio;
    }

    public void setHitMissRatio(String hitMissRatio) {
        this.hitMissRatio = hitMissRatio;
    }
}
