package megamek.server.reports;

import megamek.common.Report;

import java.util.List;
import java.util.Vector;

public class ParsedReports {
    private final List<WeaponsFireReport> weaponsFireReports;
    private final List<WeaponReport> weaponReports;
    private final Vector<Report> vPhaseReport;

    public ParsedReports(List<WeaponsFireReport> weaponsFireReports, List<WeaponReport> weaponReports,
                         Vector<Report> vPhaseReport) {
        this.weaponsFireReports = weaponsFireReports;
        this.weaponReports = weaponReports;
        this.vPhaseReport = vPhaseReport;
    }

    public List<WeaponsFireReport> getWeaponsFireReports() {
        return weaponsFireReports;
    }

    public List<WeaponReport> getWeaponReports() {
        return weaponReports;
    }

    public Vector<Report> getVPhaseReport() {
        return vPhaseReport;
    }
}
