package megamek.server.reports;

import java.util.ArrayList;
import java.util.List;

public class WeaponReport {
    private int weaponsFireReportKey;
    private final int start;
    private int end;
    private String weaponType;
    private String playerAttacked;
    private String unitAttacked;
    private String attackerPlayerName;
    private String attackerUnit;
    private boolean hit;
    private int neededToHit;
    private int hitRoll;
    private final List<Integer> damages = new ArrayList<>();
    private final List<String> hitLocations = new ArrayList<>();

    /** Physical Attack Messages
        start                4005 index
        end                  4045 or 4035
        attackerPlayerName   4005[0]
        attackerUnit         4005[1]
        weaponType           4145[0]
        playerAttacked       6065[0]
        unitAttacked         6065[1]
        neededToHit          4025[0]
        hitRoll              4025[1]
        hit                  4045 means hit, 4035 means miss
        hitLocation          6065[3]
        totalDamage          6065[2]
    */

    public WeaponReport(int weaponsFireReportKey, int start, int end, String weaponType, String playerAttacked,
                        String unitAttacked, String attackerPlayerName, String attackerUnit, boolean hit) {
        this.weaponsFireReportKey = weaponsFireReportKey;
        this.start = start;
        this.end = end;
        this.weaponType = weaponType;
        this.playerAttacked = playerAttacked;
        this.unitAttacked = unitAttacked;
        this.attackerPlayerName = attackerPlayerName;
        this.attackerUnit = attackerUnit;
        this.hit = hit;
    }

    public WeaponReport(int start, int end, String weaponType, String playerAttacked, String unitAttacked,
                        String attackerPlayerName, String attackerUnit) {
        this.start = start;
        this.end = end;
        this.weaponType = weaponType;
        this.playerAttacked = playerAttacked;
        this.unitAttacked = unitAttacked;
        this.attackerPlayerName = attackerPlayerName;
        this.attackerUnit = attackerUnit;
    }

    public int getWeaponsFireReportKey() {
        return weaponsFireReportKey;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public String getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(String weaponType) {
        this.weaponType = weaponType;
    }

    public String getPlayerAttacked() {
        return playerAttacked;
    }

    public String getUnitAttacked() {
        return unitAttacked;
    }

    public String getAttackerPlayerName() {
        return attackerPlayerName;
    }

    public String getAttackerUnit() {
        return attackerUnit;
    }

    public boolean isHit() {
        return hit;
    }

    public void setHit(boolean hit) { this.hit = hit; }

    public int getNeededToHit() {
        return neededToHit;
    }

    public void setNeededToHit(int neededToHit) {
        this.neededToHit = neededToHit;
    }

    public int getHitRoll() {
        return hitRoll;
    }

    public void setHitRoll(int hitRoll) {
        this.hitRoll = hitRoll;
    }

    // TODO: Create a total damage and set when closed? Or create a total damage function that adds up getDamages
    public List<Integer> getDamages() {
        return damages;
    }

    public List<String> getHitLocations() {
        return hitLocations;
    }

}
