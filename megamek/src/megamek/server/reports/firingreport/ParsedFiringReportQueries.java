package megamek.server.reports.firingreport;

import megamek.server.reports.ParsedReports;
import megamek.server.reports.WeaponReport;
import megamek.server.reports.WeaponsFireReport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ParsedFiringReportQueries {

    /**
     * Gets all weapons fire reports with attacks from playerName
     * @param parsed ParsedFiringReport
     * @param playerName playerName
     * @return List of WeaponsFireReport with attacks from playerName
     */
    public List<WeaponsFireReport> getPlayerAttacks(ParsedReports parsed, String playerName) {
        List<WeaponsFireReport> playerAttacks = new ArrayList<>();

        for(WeaponsFireReport weaponsFireReport : parsed.getWeaponsFireReports()) {
            if (weaponsFireReport.getPlayerFiring().contains(playerName)) {
                playerAttacks.add(weaponsFireReport);
            }
        }
        return playerAttacks;
    }

    /**
     * Gets all weapons fire reports with attacks at playerName
     * @param parsed ParsedFiringReport
     * @param playerName playerName
     * @return List of WeaponsFireReport with attacks at playerName
     */
    public List<WeaponsFireReport> getAttacksOnPlayer(ParsedReports parsed, String playerName) {
        List<WeaponsFireReport> attacksOnPlayer = new ArrayList<>();
        List<Integer> weaponsFireReportKeys = new ArrayList<>();

        for(WeaponReport weaponReport : parsed.getWeaponReports()) {
            if (weaponReport.getPlayerAttacked().contains(playerName)) {
                int weaponsFireReportKey = weaponReport.getWeaponsFireReportKey();
                if (!weaponsFireReportKeys.contains(weaponsFireReportKey)) {
                    weaponsFireReportKeys.add(weaponsFireReportKey);
                }
            }
        }
        for(Integer key : weaponsFireReportKeys) {
            attacksOnPlayer.add(parsed.getWeaponsFireReports().get(key));
        }

        return attacksOnPlayer;
    }

    /**
     * Gets a Map containing units and a list of Weapon Reports firing on that unit
     *
     * @param parsed ParsedFiringReport
     * @return Map of key:unit and value:list of Weapon Reports firing on the unit
     */
    public Map<String, List<WeaponReport>> getWeaponReportsByAttackedUnitMap(ParsedReports parsed) {
        Map<String, List<WeaponReport>> weaponReportsMap = new HashMap<>();

        for(WeaponReport weaponReport : parsed.getWeaponReports()) {
            List<WeaponReport> weaponReports = weaponReportsMap.
                    computeIfAbsent(weaponReport.getUnitAttacked(), k -> new ArrayList<>());
            weaponReports.add(weaponReport);
        }

        return weaponReportsMap;
    }

    public int getTotalDamageDone(Map<Integer, ParsedReports> parsedFiringReports) {
        int totalDamage = 0;
        for(ParsedReports parsed : parsedFiringReports.values()) {
            if (parsed.getWeaponReports() != null) {
                for(WeaponReport weaponReport : parsed.getWeaponReports())
                totalDamage += weaponReport.getDamages().stream().reduce(0,Integer::sum);
            }
        }

        return totalDamage;
    }
}
