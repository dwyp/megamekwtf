package megamek.server.reports.firingreport;

import megamek.common.Report;
import megamek.server.reports.ParsedReports;
import megamek.server.reports.WeaponReport;

import java.util.*;

public class FiringReportByAttackedUnit {
    private final StringBuilder sb = new StringBuilder();

    /**
     * Adds weapons fire reports showing all weapons fire at a unit to the bottom of the Firing Phase Report
     * @param querier
     * @param orderedReports
     * @param parsed
     * @param playerName
     */
    public void appendWeaponReportsByTarget(ParsedFiringReportQueries querier, Vector<Report> orderedReports,
                                            ParsedReports parsed, String playerName) {

        Map<String, List<WeaponReport>> weaponReportsByUnitAttackedMap = querier.getWeaponReportsByAttackedUnitMap(parsed);
        Vector<Report> weaponReportsAtPlayer = new Vector<>();
        Vector<Report> weaponReportsAtOthers = new Vector<>();

        for(String unitAttacked : weaponReportsByUnitAttackedMap.keySet()) {
            // get weapon attacks aimed at player
            if (weaponReportsByUnitAttackedMap.get(unitAttacked).get(0).getPlayerAttacked().contains(playerName)) {
                addReports( weaponReportsAtPlayer, parsed.getVPhaseReport(), weaponReportsByUnitAttackedMap.get(unitAttacked));
            } else { // get all other weapon attacks
                addReports(weaponReportsAtOthers, parsed.getVPhaseReport(), weaponReportsByUnitAttackedMap.get(unitAttacked));
            }
        }

        if (weaponReportsAtOthers.size() > 0 || weaponReportsAtPlayer.size() > 0) {
            orderedReports.add(getSeparatorReport());
        }
        orderedReports.addAll(weaponReportsAtPlayer);
        orderedReports.addAll(weaponReportsAtOthers);
    }

    private void addReports(Vector<Report> reportsToAddTo, Vector<Report> vPhaseReport,
                            List<WeaponReport> weaponReportsForAttackedUnit) {
        WeaponReport wr = weaponReportsForAttackedUnit.get(0);

        // ("Player"): "Unit" received weapons fire: Total Damage: Hits: Misses: Hit/Miss Ratio:
        Report r = new Report(3099, Report.PUBLIC);
        if (wr.getPlayerAttacked().equals(wr.getUnitAttacked())) {
            // "Unit" received weapons fire:
            r.messageId = 3098;
        } else {
            r.add(wr.getPlayerAttacked());
        }
        r.add(wr.getUnitAttacked());
        addStats(r, weaponReportsForAttackedUnit);
        reportsToAddTo.add(r);
        reportsToAddTo.addAll(createWeaponsFireReport(vPhaseReport, weaponReportsForAttackedUnit));
    }

    private void addStats(Report r, List<WeaponReport> weaponReports) {
        int totalDamage = 0;
        int hits = 0;
        int misses = 0;
        for(WeaponReport weaponReport : weaponReports) {
            totalDamage += weaponReport.getDamages().stream().reduce(0, Integer::sum);
            if (weaponReport.isHit()) {
                hits++;
            } else {
                misses++;
            }
        }
        r.add(totalDamage);
        r.add(hits);
        r.add(misses);
        int ratio = hits * 100 / (hits + misses);
        r.add(ratio / 100 + "." + ratio % 100);
    }

    private Vector<Report> createWeaponsFireReport(Vector<Report> vPhaseReport, List<WeaponReport> weaponReports) {
        Vector<Report> weaponsFireReport = new Vector<>();
        Report r;
        for(WeaponReport weaponReport : weaponReports) {
            // Check reportId of weaponReport.getStart
            if (vPhaseReport.get(weaponReport.getStart()).messageId == 6480) {

            } else {
                // Hit: or Miss:
                weaponsFireReport.add(vPhaseReport.get(weaponReport.getStart()));
                // "Weapon" fired by ("Player"): "Unit";
                r = new Report(3114, Report.PUBLIC);
                r.add(vPhaseReport.get(weaponReport.getStart() + 1).getTagData(0));
                r.add(weaponReport.getAttackerPlayerName());
                r.add(weaponReport.getAttackerUnit());
                r.indent();
                r.newlines = 0;
                weaponsFireReport.add(r);
                // Rest of report
                for (int index = weaponReport.getStart() + 2; index <= weaponReport.getEnd(); index++) {
                    weaponsFireReport.add(vPhaseReport.get(index));
                }
            }
        }
        return weaponsFireReport;
    }

    private Report getSeparatorReport() {
        Report r = new Report(1230, Report.PUBLIC);
        sb.setLength(0);
        sb.append("\n<b style=\"color:yellow; background-color:black;\">========================================================================================================================================================\n");
        sb.append("========================================================================================================================================================</b>\n");
        r.add(sb.toString());
        return r;
    }
}
