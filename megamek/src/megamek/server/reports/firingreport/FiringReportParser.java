package megamek.server.reports.firingreport;

import megamek.common.Report;
import megamek.server.reports.ParsedReports;
import megamek.server.reports.WeaponReport;
import megamek.server.reports.WeaponsFireReport;

import java.util.*;

public class FiringReportParser {

    private static final int OPENED_REPORT = -9999;
    private List<WeaponsFireReport> weaponsFireReports;
    private List<WeaponReport> weaponReports;
    private Vector<Report> vPhaseReport;

    private WeaponReport lastWeaponReport;
    private WeaponsFireReport lastWeaponsFireReport;
    private int skip6065;

    public void addParsedFiringReport(Map<Integer, ParsedReports> parsedFiringReports,
                                      Vector<Report> vPhaseReport, int round) {
        if (vPhaseReport.get(0).messageId != 3000 && vPhaseReport.get(0).messageId != 9095) { return; }
        parseFiringReport(vPhaseReport);
        parsedFiringReports.put(round, new ParsedReports(weaponsFireReports, weaponReports, vPhaseReport));
    }

    private void parseFiringReport(Vector<Report> vPhaseReport) {
        weaponsFireReports = new ArrayList<>();
        weaponReports = new ArrayList<>();
        this.vPhaseReport = vPhaseReport;
        skip6065 = 0;

        for(int reportIndex = 0; reportIndex < vPhaseReport.size(); reportIndex++) {
            Report report = vPhaseReport.get(reportIndex);
            switch (report.messageId) {
                // Strafing and weapons fire headers
                case 3101 :
                case 3100 : openWeaponsFireReport(report, reportIndex);
                            break;

                // Hit/Miss: with next report being <data> at (<data>): <B><data></B>;
                case 3222 : openWeaponReport(vPhaseReport.get(reportIndex + 1), reportIndex, true);
                            break;
                case 3221 : openWeaponReport(vPhaseReport.get(reportIndex + 1), reportIndex, false);
                            break;

                // 6065=<data> (<data>) takes <data> damage to <data>.
                case 6065 : recordWeaponDamage(report);
                            break;

                // 3150=\ needs <data>,
                case 3150 : recordNeededToHit(report);
                            break;

                // 3155=rolls <data> :
                case 3155 : recordRoll(report);
                            break;

                // 6390=<data> EXPLODES!  <data> DAMAGE! or 6130=<data> damage transfers to <data> or
                // 9005=\ <data> damage transfers to the SI. <data> SI remaining
                case 6390 :
                case 6130 :
                case 9005 : skip6065++;
                            break;
                // Piloting rolls
                case 4140 :
                case 2275 :
                case 2280 : closeReports(reportIndex);
                            break;
            }
        }
        closeReports(vPhaseReport.size());
    }

    /** Weapon Report handling */
    private void openWeaponReport(Report report, int reportIndex, boolean hit) {
        closeWeaponReport(reportIndex);
        int weaponsFireReportKey = lastWeaponsFireReport.getWeaponsFireReportKey();
        String weaponType = (report.getTagData(0));

        String playerName;
        String unit;
        if(report.messageId == 3116) {
            // Ammo type in report at [1]
             playerName = report.getTagData(2);
            // if no data tag in [3], use tag data in [2] (which is really the unit anyways)
            unit = (report.getTagDataSize() > 3) ? report.getTagData(3) : report.getTagData(2);
        } else{
            playerName = report.getTagData(1);
            // if no data tag in [2], use tag data in [1] (which is really the unit anyways)
            unit = (report.getTagDataSize() > 2) ? report.getTagData(2) : report.getTagData(1);
        }

        String shooterPlayerName = weaponsFireReports.get(weaponsFireReportKey).getPlayerFiring();
        String shooterUnit = weaponsFireReports.get(weaponsFireReportKey).getUnitFiring();

        WeaponReport weaponReport = new WeaponReport(weaponsFireReportKey, reportIndex,
                OPENED_REPORT, weaponType, playerName, unit, shooterPlayerName, shooterUnit, hit);

        lastWeaponReport = weaponReport;
        weaponReports.add(weaponReport);
    }

    private void recordWeaponDamage(Report report) {
        if (skip6065 == 0) {
            if(lastWeaponReport.getEnd() == OPENED_REPORT) {
                lastWeaponReport.getDamages().add(Integer.valueOf(report.getTagData(2)));
                lastWeaponReport.getHitLocations().add(report.getTagData(3));
            }
        } else { skip6065--; }
    }

    private void recordNeededToHit(Report report) {
        lastWeaponReport.setNeededToHit(Integer.parseInt(report.getTagData(0)));
    }

    private void recordRoll(Report report) {
        lastWeaponReport.setHitRoll(Integer.parseInt(report.getTagData(0)));
    }

    private void closeWeaponReport(int reportIndex) {
        if (!weaponReports.isEmpty()) {
            if (lastWeaponReport.getEnd() == OPENED_REPORT) {
                lastWeaponReport.setEnd(reportIndex - 1);
            }
        }
    }

    /** Weapons Fire Report handling */
    private void openWeaponsFireReport(Report report, int reportIndex) {
        closeReports(reportIndex);
        String playerName = report.getTagData(0);
        String unit = report.getTagData(1);
        WeaponsFireReport weaponsFireReport = new WeaponsFireReport(weaponsFireReports.size(), reportIndex, OPENED_REPORT, playerName, unit);

        lastWeaponsFireReport = weaponsFireReport;
        weaponsFireReports.add(weaponsFireReport);
    }

    private void closeWeaponsFireReport(int reportIndex) {
        if (!weaponsFireReports.isEmpty()) {
            if (lastWeaponsFireReport.getEnd() == OPENED_REPORT) {
                lastWeaponsFireReport.setEnd(reportIndex - 1);
                List<WeaponReport> wrs = getWeaponReportsForWeaponsFireReport(lastWeaponsFireReport.getWeaponsFireReportKey());
                wrs.forEach(k -> setWeaponsFireReportStats(lastWeaponsFireReport, k));
                lastWeaponsFireReport.setHitMissRatio(getHitMissRatio(lastWeaponsFireReport));
            }
            addStatsToWeaponsFireReport(vPhaseReport.get(lastWeaponsFireReport.getStart()), lastWeaponsFireReport);
        }
    }

    private List<WeaponReport> getWeaponReportsForWeaponsFireReport(int weaponsFireReportKey) {
        List<WeaponReport> wrsForWeaponsFireReport = new ArrayList<>();
        for(int index = weaponReports.size() - 1; index >= 0 && weaponReports.get(index).getWeaponsFireReportKey() == weaponsFireReportKey; index--) {
            wrsForWeaponsFireReport.add(weaponReports.get(index));
        }
        return wrsForWeaponsFireReport;
    }

    /** Weapons Fire Report stats */
    private void setWeaponsFireReportStats(WeaponsFireReport weaponsFireReport, WeaponReport wr) {
        if (wr.isHit()) {
            weaponsFireReport.addHits(1);
        } else {
            weaponsFireReport.addMisses(1);
        }
        weaponsFireReport.addDamage(wr.getDamages().stream().reduce(0,Integer::sum));
    }

    private String getHitMissRatio(WeaponsFireReport ar) {
        int ratio = ar.getHits() * 100 / (ar.getHits() + ar.getMisses());
        return ratio / 100 + "." + ratio % 100;
    }

    private void addStatsToWeaponsFireReport(Report report, WeaponsFireReport ar) {
        report.add(ar.getTotalDamage());
        report.add(ar.getHits());
        report.add(ar.getMisses());
        report.add(ar.getHitMissRatio());
    }

    /** Set end index for both lastWeaponsFireReport and lastWeaponReport */
    private void closeReports(int reportIndex) {
        closeWeaponReport(reportIndex);
        closeWeaponsFireReport(reportIndex);
    }
}
