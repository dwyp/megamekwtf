package megamek.server.reports.firingreport;

import megamek.common.Report;
import megamek.server.reports.ParsedReports;
import megamek.server.reports.ReportsToString;
import megamek.server.reports.WeaponsFireReport;

import java.util.*;

public class FiringReportOrderPrioritizer {

    private final ParsedFiringReportQueries querier = new ParsedFiringReportQueries();
    private final FiringReportByAttackedUnit appender = new FiringReportByAttackedUnit();
    private boolean[] included;

    public Vector<Report> getReportWithPlayerPrioritized(ParsedReports parsedReports, String playerName) {
        Vector<Report> orderedReport = new Vector<>();
        Vector<Report> vPhaseReport = parsedReports.getVPhaseReport();

        if (vPhaseReport.get(0).messageId != 3000 && vPhaseReport.get(0).messageId != 9095) return vPhaseReport;

        included = new boolean[vPhaseReport.size()];

        for(int index = 0; vPhaseReport.get(index).messageId == 3000 || vPhaseReport.get(index).messageId == 9095; index++) {
            orderedReport.add(vPhaseReport.get(index));
            included[index] = true;
        }

        // add player attacks
        List<WeaponsFireReport> playerAttacks = querier.getPlayerAttacks(parsedReports, playerName);
        for(WeaponsFireReport weaponsFireReport : playerAttacks) {
            orderedReport.addAll(getReports(vPhaseReport, weaponsFireReport));
        }

        // add attacks on player
        List<WeaponsFireReport> attacksOnPlayer = querier.getAttacksOnPlayer(parsedReports, playerName);
        for(WeaponsFireReport weaponsFireReport : attacksOnPlayer) {
            orderedReport.addAll(getReports(vPhaseReport, weaponsFireReport));
        }

        // add everything else
        for(int index = 0; index < vPhaseReport.size(); index++) {
            if (!included[index]) {
                orderedReport.add(vPhaseReport.get(index));
                included[index] = true;
            }
        }
        // append weapon attacks by target
        appender.appendWeaponReportsByTarget(querier, orderedReport, parsedReports, playerName);

        // convert whole report to string and place in a report
        return ReportsToString.getStringReports(orderedReport);
    }

    private Vector<Report> getReports(Vector<Report> reports, WeaponsFireReport ar) {
        Vector<Report> addReports = new Vector<>();

        for(int reportIndex = ar.getStart(); reportIndex <= ar.getEnd(); reportIndex++) {
            addReports.add(reports.get(reportIndex));
            included[reportIndex] = true;
        }
        return addReports;
    }
}