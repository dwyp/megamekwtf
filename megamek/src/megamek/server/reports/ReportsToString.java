package megamek.server.reports;

import megamek.common.Report;

import java.util.Vector;

public class ReportsToString {
    private static final StringBuilder sb = new StringBuilder();

    public static Vector<Report> getStringReports(Vector<Report> orderedReport) {
        Vector<Report> stringReports = new Vector<>();
        Report stringReport = new Report(1230, Report.PUBLIC);
        stringReport.add(convertReportsToString(orderedReport));
        stringReports.add(stringReport);
        return stringReports;
    }

    private static String convertReportsToString(Vector<Report> reports) {
        sb.setLength(0);
        for(Report report : reports) {
            sb.append(report.getText());
        }

        return sb.toString();
    }
}
