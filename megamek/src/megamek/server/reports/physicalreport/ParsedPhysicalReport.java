package megamek.server.reports.physicalreport;

import megamek.common.Report;
import megamek.server.reports.WeaponReport;

import java.util.List;
import java.util.Vector;

// TODO: remove and replace with newly created ParsedReports
public class ParsedPhysicalReport {
    private final List<WeaponReport> weaponReports;
    private final Vector<Report> vPhaseReport;

    public ParsedPhysicalReport(List<WeaponReport> weaponReports, Vector<Report> vPhaseReport) {
        this.weaponReports = weaponReports;
        this.vPhaseReport = vPhaseReport;
    }

    public List<WeaponReport> getWeaponReports() {
        return weaponReports;
    }

    public Vector<Report> getVPhaseReport() {
        return vPhaseReport;
    }
}
