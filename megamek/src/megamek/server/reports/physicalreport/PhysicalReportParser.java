package megamek.server.reports.physicalreport;

import megamek.common.Report;
import megamek.server.reports.WeaponReport;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class PhysicalReportParser {

    private static final int OPENED_REPORT = -9999;
    private List<WeaponReport> weaponReports;
    private String lastAttackerUnit;
    private String lastAttackerPlayerName;
    private WeaponReport lastWeaponReport;
    private int skip6065;

    public void addParsedPhysicalReports(Map<Integer, ParsedPhysicalReport> parsed, Vector<Report> vPhaseReport, int round) {
        if (vPhaseReport.get(0).messageId != 4000) {
            return;
        }
        parseWeaponReport(vPhaseReport);

        parsed.put(round, new ParsedPhysicalReport(weaponReports, vPhaseReport));
    }

    // TODO: Add WeaponsFireReport handling
    private void parseWeaponReport(Vector<Report> vPhaseReport) {
        weaponReports = new ArrayList<>();
        skip6065 = 0;

        for(int index = 0; index < vPhaseReport.size(); index++) {
            Report report = vPhaseReport.get(index);
            switch(report.messageId) {
                case 4005 : setLastAttacker(report);
                            break;
                case 4010 : openWeaponReport(report, "Punch", index);
                            break;
                case 4055 : openWeaponReport(report, "Kick", index);
                            break;
                case 4145 : openWeaponReport(report, report.getTagData(0), index);
                            break;
                case 4025 : setNeededToHitAndToHit(report);
                            break;
                case 4040 :
                case 4045 : setIsHit(true);
                            break;
                case 4035 : setIsHit(false);
                            break;
                case 6065 : setDamageAndHitLocation(report);
                            break;
                case 6390 :
                case 6130 :
                case 9005 : skip6065++;
                            break;
                case 4140 :
                case 2275 :
                case 2280 : closeWeaponReport(index);
            }
        }
        closeWeaponReport(vPhaseReport.size() - 1);
    }

    private void setLastAttacker(Report report) {
        lastAttackerPlayerName = report.getTagData(0);
        lastAttackerUnit = report.getTagData(1);
    }

    private void openWeaponReport(Report report, String weaponType, int index) {
        String playerAttacked = report.getTagData(1);
        String unitAttacked = (report.getTagDataSize() == 3) ? report.getTagData(2) : playerAttacked;

        WeaponReport wr = new WeaponReport(index, OPENED_REPORT, weaponType, playerAttacked, unitAttacked,
                lastAttackerPlayerName, lastAttackerUnit);

        weaponReports.add(wr);
        lastWeaponReport = wr;
    };

    private void setNeededToHitAndToHit(Report report) {
        lastWeaponReport.setNeededToHit(Integer.parseInt(report.getTagData(0)));
        lastWeaponReport.setHitRoll(Integer.parseInt(report.getTagData(1)));
    }

    private void setIsHit(boolean isHit) {
        lastWeaponReport.setHit(isHit);
    }

    private void setDamageAndHitLocation(Report report) {
        if (skip6065 == 0) {
            if(lastWeaponReport.getEnd() == OPENED_REPORT) {
                lastWeaponReport.getDamages().add(Integer.parseInt(report.getTagData(2)));
                lastWeaponReport.getHitLocations().add(report.getTagData(3));
            }
        } else { skip6065--; }
    }

    private void closeWeaponReport(int index) {
        if (lastWeaponReport != null) {
            if (lastWeaponReport.getEnd() == OPENED_REPORT) {
                lastWeaponReport.setEnd(index - 1);
            }
        }
    }
}
